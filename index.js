const { Telegraf } = require('telegraf');
const SpamWatchApi = require('spamwatch');
const config = require('./botConfig')

const useEnv = process.env.ENABLE_ENV_MODE
if (useEnv == "true") {
    const BotToken = process.env.TELEGRAM_BOT_TOKEN;
          SpamWatchApiToken = process.env.SPAMWATCH_API_TOKEN;
          SpamWatchApiHost = process.env.SPAMWATCH_API_HOST;
} else {
    const BotToken = config.botToken;
    const SpamWatchApiToken = config.spamwatch.token;
    const SpamWatchApiHost = config.spamwatch.host
}


const bot = new Telegraf(BotToken)

bot.start((ctx) => {
    ctx.reply('Hello, world! I am alive.')
})

bot.launch().then(() => {
    console.log('@${bot.context.botInfo.username} is running...')
})