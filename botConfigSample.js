/**
 * @type {Config}
 */
const config = {
    spamwatch: {
        /**
         * @type {!string}
         * API Token for accessing the API.
         * 
         * Get one from Simon if you don't have.
         */
        token: 'yourApiTokenHere',

        /**
         * @type {!string}
         * Endpoint to the API server.
         * 
         * By default, we lock our API wrapper to the defaults.
         */
        host: 'https://api.spamwat.ch'
    },

  /**
   * @type {!string}
   * Telegram Bot token obtained from https://t.me/BotFather.
   * DO NOT COMMIT YOUR BOT TOKEN!
   */
    token: 'yourTgTokenHere!' // Get one from BotFather if you don't have one.
}

module.exports = Object.freeze(config);