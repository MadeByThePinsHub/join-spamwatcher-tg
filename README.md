# Join SpamWatcher Telegram Bot

The bot is currently under development and it will be soon be available to the general public.

## How to use?

### Hosted Solution
1. [Add the bot](https://telegrm.dog/JoinSpamWatcher_bot?startgroup=true) to your group as an admin with only `Ban users` permission. (The bot will do nothing if you add any other permissions.)
2. As soon as new member joins your group, the bot server will connect to the SpamWatch API servers and queries that new member's user ID against the blacklist. The checks should take up to 30 seconds.
    - If the bot found that user was banned, the bot will automatically bans the user and caches it's response to speed up the process.
    - If not, the bot will do nothing.

### Self-hosted Solution